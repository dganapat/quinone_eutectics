import os
import sys
import copy
import numpy as np
import scipy.optimize as sciop
import math
import pandas as pd
from itertools import combinations

##########################
##### NOTES
##########################

# This code is used to calculate the approximate eutectic composition and temperature for an n-component mixture
# TODO: Add more notes about how to use this code
# TODO: It looks like we need another subfolder, "input"

folder = os.path.dirname(os.path.realpath(__file__))
analysisfilefolder = folder + '/Analysis_Files'

# physical constants
R = 8.314 # J/mol/K
kb = 0.008314 #kJ/molK


#class used as input to solver:
class equil:

    def __init__(self, tempList, enthList, mixCompList):
		#Initilization sets up parameters for calculation
		#Length of list = number of components in system
		#ordering of tempList and enthList must be consistent
        self.tempList = copy.copy(tempList) #List of melting temperatures for components being calculated
        self.enthList = copy.copy(enthList) #List of enthalpies for components being calculated
        self.mixCompList = copy.copy(mixCompList) #List of components (entries in the overall component list which is sorted by temperature)
        return

    def __call__(self, x):
		#What the solver calls
		#x[0] to x[-2]: mol fractions of components
		#x[-1]: temperature

		#Initializing output data
		#outData[0] to outData[-2] equations: chemical potentials between solid and liquid phase must be equal
		#outData[-1] equation: all mole fractions add up to 1.0
        outData = np.zeros( len(x) )

		#Looping through mole fractions
        for counti, i in enumerate(self.mixCompList):

			#Calculating activity coefficient using mole fractions and interaction parameter
            lnGam = 0.0
            for countj, j in enumerate(self.mixCompList):
                if j != i:
                    lnGam += AData[i][j]*x[countj]*(1.0-x[counti])
                    for countk, k in enumerate(self.mixCompList):
                        if (j != k) and (i != k):
                            lnGam += -0.5*AData[j][k]*x[countj]*x[countk]
			#Calculating function output
            outData[counti] = math.exp( -self.enthList[i]/kb*(1.0/x[-1] - 1.0/self.tempList[i]) ) - x[counti]*math.exp(lnGam/(kb*x[-1]*1000.0))

            outData[-1] += x[counti]

        outData[-1] += -1

        return outData


##########################
##### INPUTS
##########################

# read temp and enthalpy values from .csv files

# file for single components (melting point and enthalpy of fusion)
# TODO: Make single input-output folder (this input is the output from the analyze_dsc_for_pub.py)
singledf = pd.read_csv(folder + '/Analysis_Files/quinones_OLD.csv')

# file for interaction parameters
binarydf = pd.read_csv(folder + '/Analysis_Files/L_out.csv')

# components to find eutectics 
comps_of_interest = ['Q2','Q5','Q6']

# orders of eutectic to find (number of components)
# For example: to predict both the binary and ternary mixtures ns = [2,3]
ns = [2,3]

####################
#### Change the name of the outfile_path 
####################
#output file
outfile_path = folder+'/Analysis_Files/eutectic_out_20211118.csv'


##########################
##### CALCULATIONS
##########################

singledf['mp_K'] = singledf['mp_C']+273.15

# get lists of the temperatures, enthalpies, and component names, all sorted by mp
tList = singledf[singledf.comp.isin(comps_of_interest)].sort_values('mp_K')['mp_K'].tolist()
hList = [Hfus/(1000.) for Hfus in singledf[singledf.comp.isin(comps_of_interest)].sort_values('mp_K')['Hfus'].tolist()]
compList = singledf[singledf.comp.isin(comps_of_interest)].sort_values('mp_K')['comp'].tolist()

# combinations to test
# batchList is a list of, for each n, the mixtures of that n
batchList = [[]]
for n in ns:
    if n > len(comps_of_interest):
        sys.exit('Error: n cannot be greater than the number of components of interest')
    batchList.append([foo for foo in combinations(range(len(compList)),n)])


# Read in interaction energies into a symmetric matrix, ordered by melting point
AData = []
for comp1 in compList:
    interaction_params = []
    for comp2 in compList:
        # interaction_params.append(0) #uncomment/comment for ideal solution
        # TODO: above line is the only line needed for an ideal solution. Add variable at top of script selecting whether to run ideal or regular solution model calculation
        if comp1==comp2:
            interaction_params.append(0)
        else:
            # note the order of the components in the file is random, so which one is comp1 or comp2 has to be checked
            if len(binarydf[(binarydf.comp1==comp1)&(binarydf.comp2==comp2)]) > 0:
                interaction_params.append(binarydf[(binarydf.comp1==comp1)&(binarydf.comp2==comp2)].L_model.values[0])
            else:
                interaction_params.append(binarydf[(binarydf.comp2==comp1)&(binarydf.comp1==comp2)].L_model.values[0])
    AData.append(interaction_params)


eutectic_lst = []

#Looping through every composition
for i in range(len(batchList)):
   
    #On first loop, use melting temps for 1 component systems
    if i == 0:
        for t,c in zip(tList,compList):
            eutectic_lst.append({'n':1,'T_e':t-273.15,'components':c})
    else:
        #Initializing mol fraction and eutectic temperatures
        xLen = len(batchList[i][0])
        xData = np.zeros( xLen+1 )
        for j in range(len(batchList[i])):
            xData[j] = 1.0/float(xLen)
            xData[-1] = min(tList[x] for x in batchList[i][j])

 			#Solving			
            newFunc = equil(tList, hList, batchList[i][j])
            result, infodict, ier, mesg = sciop.fsolve(newFunc, xData, full_output=True)
            newentry = {'n':xLen,'T_e':result[-1]-273.15,'components':','.join([compList[q] for q in batchList[i][j]]),'composition':list(result[0:-1]),'infodict':infodict,'ier':ier,'mesg':mesg }
            eutectic_lst.append(newentry)
            newFunc = []            

eutectics = pd.DataFrame(eutectic_lst)
eutectics = eutectics.sort_values(by=['T_e'])
eutectics.to_csv(outfile_path)






