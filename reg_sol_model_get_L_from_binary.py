
# New made on 7/16/20
# Original file seems to be 20180928 immiscible model fit.py

import os
import time
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import fsolve

##########################
##### NOTES
##########################

# This code is used to calculate the interaction parameter, L, for a binary quinone (or hydroquinone) mixture

starttime= time.time()
folder = os.path.dirname(os.path.realpath(__file__))
analysisfilefolder = folder + '/Analysis_Files'

##########################
##### INPUTS
##########################
# Type of analysis 
# 1 = single, or 
# 2 = loop from spreadsheets
analysis_type = 2

# physical constants
R = 8.314 # J/mol/K

# Inputs for analysis type 1
Aname = 'Q6'
Hmelt_A =  -18611.894530000001   # J/mol
Tmelt_A = 393.84296729999994
Bname = 'Q2'
Hmelt_B = -17447.37977 
Tmelt_B = 317.46808540999996
Te = 310.44587120999995

# Initial guesses for analysis type 1
Lguess = 2000 # J/mol
xeguess = 0.8


# immiscible solids model
def equations(p, *data):
    L, xe = p
    Tm_A, Hm_A, Tm_B, Hm_B, Te = data
    eq1 = Hm_B*(Te/Tm_B - 1) + R*Te*np.log(xe) + L*(1-xe)**2
    eq2 = Hm_A*(Te/Tm_A - 1) + R*Te*np.log(1-xe) + L*(xe)**2
    return eq1, eq2

if analysis_type == 1:
    data = (Tmelt_A,Hmelt_A,Tmelt_B,Hmelt_B,Te)
    result, infodict, ier, mesg = fsolve(equations, (Lguess,xeguess), args=data,full_output=True)    
    print(mesg)
    print('L: '+str(result[0]))
    print('xe: '+str(result[1]))

elif analysis_type == 2:
    # assumes this single file has columns mp_C, Hfus
    singledf = pd.read_csv(analysisfilefolder + '/quinones_OLD.csv')
    # assumes this binary files has columns T_e
    binarydf = pd.read_csv(analysisfilefolder + '/binary_inputs_OLD.csv')

    binarydf['L_model'] = ''
    binarydf['xe_model'] = ''
    Lguess = 2000 # J/mol
    xeguess = 0.5
    for i,row in binarydf.iterrows():
        Te = row.T_e + 273.15
        Tmelt_A = singledf[singledf.comp==row.comp1].mp_C.values[0] + 273.15
        Hmelt_A = -singledf[singledf.comp==row.comp1].Hfus.values[0]
        Tmelt_B = singledf[singledf.comp==row.comp2].mp_C.values[0] + 273.15
        Hmelt_B = -singledf[singledf.comp==row.comp2].Hfus.values[0]

        if row.comp1 == 'Q6' and row.comp2 == 'Q22':
            xeguess = 0.9
        
        data = (Tmelt_A,Hmelt_A,Tmelt_B,Hmelt_B,Te)                
        result, infodict, ier, mesg = fsolve(equations, (Lguess,xeguess), args=data,full_output=True)        
        
        binarydf.at[i, 'L_model'] = result[0]
        binarydf.at[i, 'xe_model'] = result[1]
        binarydf.at[i, 'mesg'] = mesg
        
    
    binarydf.to_csv(analysisfilefolder+'/L_out.csv')
    