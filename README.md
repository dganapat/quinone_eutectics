# Quinone_Eutectics

# README for Paper: Large Decrease in Melting Point of Benzoquinones via High-n Eutectic Mixing Predicted by a Regular Solution Model 
## Repository Structure
- Assumes that all of the data files are in a folder ../data/ which is organized into subfolders named with the YYYYMMDD date.
- Also assumes that there is another folder ../plots/ which has subfolders for different plots.
- Also assumes that there is another folder ../Analysis Files/


# Data Files:
 - Included are the raw data files for the Q2-Q5 Binary system (at the 50-50 composition) and the Q2-Q5-Q6 Ternary system (at the eutectic composition calculated by both the ideal solution model and the regular solution model, and including the 50-50 binary mixtures of each "pair" in this sytem)