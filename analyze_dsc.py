import DSC
import os
import time
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib import gridspec
import ast
import csv
import sys

starttime= time.time()
# TODO: Remove the parent directory path so it's in the same folder
# TODO: Rename file so it's just analyze DSC
mainfolder = os.path.dirname(os.path.realpath(__file__))
datamainfolder = mainfolder+'/Data'
# plotfolder = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))+'/pub_out'
plotfolder = mainfolder + '/Plots'
analysisfilefolder = mainfolder + '/Analysis_Files'
date_str = str(time.localtime().tm_year)+str(time.localtime().tm_mon).zfill(2)+str(time.localtime().tm_mday).zfill(2)

##########################
##### NOTES
##########################

# Folders: assumes that the 
# script and data are in separate folders in some main folder, 
# i.e. the script is in "../mainfolder/script/"
# and the data is in "../mainfolder/data/20190521/"
# (where the date for each run is a separate folder in the data folder)
# and there is another folder for outputs, "../mainfolder/pub_out/"

# TODO: Include the csv with the data files that we're including in the repository

# The input spreadsheet (spreadsheet_name) should be of the same form as the provided "DSC experiments - DSC_publish.csv"
# New lines can be added to this spreadsheet with new DSC data files
# To analyze new DSC data files, add the data files to the spreadsheet in the same way as the existing columns



##########################
##### INPUTS
##########################

# Input type: either put 'single' for analysis of a single run,
# or 'loop' for analysis of all the runs in the spreadsheet 
input_type = 'loop'

# Fill in the filename if looking at a single input
the_filename='20200903_Q2-Q4-Q5_1-1-1.txt'


# If you want to play around with the fit parameters here, set this to True
# (Only matters for input_type = single)
# Otherwise, if False, the fit parameters in the spreadsheet will be used, and if there is nothing
# there, then the default will be 1 for the fit_nums and 0.001 for s
use_these_fit_parameters = False
fit_num_left = 8
fit_num_right =1
s = 0.001

# Input list

#### If you want to run all of the experiments in the DSC experiments - DSC_publish.csv, comment out the below line uncomment the "filenamelist = 'all'" line. ####
#### If you only want to run a select few experiments, write them in a txt file and include that file in the main folder of this repository (same level this scipt)
#### This file goes in the Analysis_Files subfolder
# filenamelist = 'list_publish_select.txt'
filenamelist = 'all'

# Input spreadsheet - this is the spreadsheet that contains all the information about the DSC experiments. An example file is included
spreadsheet_name = 'DSC experiments - DSC_publish.csv'

# Output spreadsheet - this stores all the results from this analysis so it can be used to calculate interaction parameters with the other scripts.
# It will be saved in the Analysis_Files folder
output_spreadsheet = 'DSC_publish_results_'+date_str+'.csv'


##########################
##### Data import and analysis
##########################

spreadsheet = DSC.import_spreadsheet(mainfolder+'/',spreadsheet_name)
if input_type == 'loop':
    with open(analysisfilefolder +'/'+output_spreadsheet,'w') as f:
        writer = csv.writer(f)
        writer.writerow(['Title','Filename','Process','Peaks','Eutectic_peak','mp_C','Hm','H_eutectic'])

if input_type == 'single':
    filenames = [the_filename]
elif input_type == 'loop':
    if filenamelist == 'all':
        filenames = [row.Filename for i,row in spreadsheet.iterrows()]
    else: 
        with open(analysisfilefolder+'/'+filenamelist) as f:
            filenames = f.read().splitlines()

# sys.exit('Stopped in program')

# TODO: Need to include a copy of the spreadsheet with the appropriate columns in the github repository, because this code assumes a certain structure
# TODO: Add example DSC files in an example DSC folder? I think it would actually be easier to change the repository structure 
# so the data files are a subfolder in the main folder. That way it's easier for users to see examples
for filename in filenames:

    # try:
        
    plt.close('all')

    # Import the relevant data from the input spreadsheet       
    filefolder=str(int(spreadsheet[spreadsheet.Filename==filename].DateFolder.values[0]))
    data = DSC.import_DSC(datamainfolder+'/'+filefolder,filename)
    
    baseline_fitting_points = ast.literal_eval(spreadsheet[spreadsheet.Filename==filename].baseline_fitting_points.values[0])
    approx_eutectic_temp = float(spreadsheet[spreadsheet.Filename==filename].approx_eutectic_temp.values[0])
    enthalpy_fitting_points = ast.literal_eval(spreadsheet[spreadsheet.Filename==filename].enthalpy_fitting_points.values[0])
    process = spreadsheet[spreadsheet.Filename==filename].Process.values[0]
    moles = spreadsheet[spreadsheet.Filename==filename].Sample_mg.values[0]/1000./spreadsheet[spreadsheet.Filename==filename].MW_avg.values[0]
    ramp = spreadsheet[spreadsheet.Filename==filename].Ramp.values[0]
    plot_eutectic_integration = True if spreadsheet[spreadsheet.Filename==filename].integrate_eutectic.values[0] == 'Y' else False
    plot_title = spreadsheet[spreadsheet.Filename==filename].plot_title.values[0]
            
    # find peaks
    peaks = DSC.find_peaks(data,num=process)
    # pick the closest peak to the "approximate eutectic" as the eutectic
    eutectic_peak = min(peaks,key=lambda x:abs(x-approx_eutectic_temp))
    # prepare the fit_nums
    if input_type == 'single':
        if not use_these_fit_parameters:
            fit_num_left = spreadsheet[spreadsheet.Filename==filename].fit_num_left.values[0]
            fit_num_right = spreadsheet[spreadsheet.Filename==filename].fit_num_right.values[0]
            fit_nums = {'left':fit_num_left if fit_num_left==fit_num_left else 1,'right':fit_num_right if fit_num_right==fit_num_right else 1}  
            s1 = spreadsheet[spreadsheet.Filename==filename].s.values[0]
            s = s1 if s1==s1 else 0.001  
        else:
            fit_nums = {'left':fit_num_left,'right':fit_num_right}
    elif input_type == 'loop': 
        fit_num_left = spreadsheet[spreadsheet.Filename==filename].fit_num_left.values[0]
        fit_num_right = spreadsheet[spreadsheet.Filename==filename].fit_num_right.values[0]
        s = spreadsheet[spreadsheet.Filename==filename].s.values[0]
        fit_nums = {'left':fit_num_left if fit_num_left==fit_num_left else 1,'right':fit_num_right if fit_num_right==fit_num_right else 1}  
    # fits at the 2nd derivative going to 0
    lin_fits,lin_fit_temps = DSC.linear_fit_peak_sides(data,num=process,peaks=[eutectic_peak],fit_nums=fit_nums,s=s if s==s else 0.001,return_temps=True)
    

    # baseline
    x = data[(data.Process==process)&((data.Temp>enthalpy_fitting_points[0])&(data.Temp<enthalpy_fitting_points[1]))].Temp
    baseline = DSC.baseline_vanderPlaats(data,x,num=process,locations=baseline_fitting_points,Tb=enthalpy_fitting_points[0],Te=enthalpy_fitting_points[1])
    
    # mp
    mp = DSC.mp_from_baseline_and_fit(x,baseline,lin_fits[0][0])
    
    # enthalpy
    enthalpy_vdp = DSC.calc_enthalpy_vanderPlaats(data,(enthalpy_fitting_points[0],enthalpy_fitting_points[1]),moles,ramp,num=process,locations=baseline_fitting_points,baseline_values=baseline)
    if plot_eutectic_integration:
        baseline_props = {'locations':baseline_fitting_points,'Tb':enthalpy_fitting_points[0],'Te':enthalpy_fitting_points[1]}
        eutectic_enthalpy,eutectic_enthalpy_pts = DSC.calc_enthalpy_peak(data,enthalpy_fitting_points[0],lin_fits,lin_fit_temps,baseline_props,ramp,num=process,return_pts=True)
    else:
        eutectic_enthalpy = np.nan
       
    print('====== File '+filename+' ======') 
    print('Process = '+str(process))
    print('Peaks found at: '+str(peaks))
    print('Eutectic peak located at: '+str(eutectic_peak)+' C (approx. eutectic was '+str(approx_eutectic_temp)+' C)')
    print('Melting point calculated: '+str(mp)+' C')
    print('enthalpy of melting = '+str(enthalpy_vdp)+' J/mol')
    if plot_eutectic_integration:
        print('enthalpy of eutectic peak = '+str(eutectic_enthalpy)+' J')
    print('Time to Run = ' + str(time.time()-starttime) + ' s')
    if input_type == 'loop':
        with open(analysisfilefolder+'/'+output_spreadsheet, 'a') as f:
            writer = csv.writer(f)
            writer.writerow([plot_title,filename,str(process),str(peaks),str(eutectic_peak),str(mp),str(enthalpy_vdp),str(eutectic_enthalpy)])

    # except:
    #     print('******************************')
    #     print('Error occurred for '+filename)
    #     if input_type == 'loop':
    #         with open(plotfolder+'/'+output_spreadsheet, 'a') as f:
    #             writer = csv.writer(f)
    #             writer.writerow(['ERROR',filename,'','','','','',''])


    try:
        
        ##########################
        ##### Data plotting
        ##########################
        
        plt.figure(figsize=(6.25,2.9), dpi=100)
        gs = gridspec.GridSpec(1, 3, wspace=0.15)
        
        ax1 = plt.subplot(gs[0])
        
        # Title
        if plot_title == plot_title:
            plt.title(plot_title,loc='left',fontsize=12)
        
        ax2 = plt.subplot(gs[1], sharey=ax1, sharex=ax1)
        ax2.get_yaxis().set_visible(False)
        axes_to_plot = [ax1,ax2]
        if plot_eutectic_integration:
            ax3 = plt.subplot(gs[2], sharey=ax1, sharex=ax1)
            ax3.get_yaxis().set_visible(False)
            axes_to_plot = [ax1,ax2,ax3]
        
        for ax in axes_to_plot:
            # plot data
            ax.plot(data[data.Process==process].Temp,data[data.Process==process].Heat/moles/1000.,linewidth=1,c=(49/255.,91/255.,138/255.))
            # plot baseline
            ax.plot(x,baseline/moles/1000.,'--',linewidth=1,c=(200/255.,91/255.,108/255.))
        
        # plot mp fits on ax1
        x_for_mp_fit = data[(data.Process==process)&((data.Temp>mp)&(data.Temp<lin_fit_temps[0][0]))]
        ax1.plot(x_for_mp_fit,np.polyval(lin_fits[0][0],x_for_mp_fit)/moles/1000.,'--',c=(243/255.,117/255.,105/255.))
        ax1.scatter(mp,np.polyval(lin_fits[0][0],mp)/moles/1000.,s=10,c='k')
        ax1.scatter(lin_fit_temps[0][0],np.polyval(lin_fits[0][0],lin_fit_temps[0][0])/moles/1000.,s=10,c='k')
        
        # plot integration on ax2
        ax2.fill_between(x,baseline/moles/1000.,data[(data.Process==process)&((data.Temp>enthalpy_fitting_points[0])&(data.Temp<enthalpy_fitting_points[1]))].Heat/moles/1000.,color=(83/255.,143/255.,204/255.))
        
        # plot eutectic integration on ax3
        if plot_eutectic_integration:
            ax3.fill_between(eutectic_enthalpy_pts[0],eutectic_enthalpy_pts[1]/moles/1000.,eutectic_enthalpy_pts[2]/moles/1000.,color=(83/255.,143/255.,204/255.))
        
        
        ### Ranges
        range_temp = spreadsheet[spreadsheet.Filename==filename].plot_range_temp.values[0]
        range_heatnormalized = spreadsheet[spreadsheet.Filename==filename].plot_range_heatnormalized.values[0]
        ## use the fact that nan != nan
        if range_temp == range_temp:
            ax1.set_xlim(ast.literal_eval(range_temp))
            # TODO: add an else statment with an error message saying that the provided temperature ranges are invalid?
        if range_heatnormalized == range_heatnormalized:
            ax1.set_ylim(ast.literal_eval(range_heatnormalized))
        
        
        ### Labels
        ax1.set_xlabel(r"Temp ($^{\circ}$C)")
        ax1.set_ylabel("Heat Flow (W/mole)\n(endotherm down)")
        
        for theaxis in axes_to_plot:
            for item in ([theaxis.xaxis.label, theaxis.yaxis.label, theaxis.yaxis.get_offset_text(), theaxis.xaxis.get_offset_text()]):
                item.set_fontsize(12)
            for item in (theaxis.get_xticklabels() + theaxis.get_yticklabels()):
                item.set_fontsize(10)
            
        plt.gcf().subplots_adjust(left=0.16,bottom=0.17,top=0.91)
        plt.savefig(plotfolder+'/'+filename[0:-4]+'_proc'+str(process)+'.png',format='png',dpi=300)# ,transparent=True)
    
        if input_type == 'single':
            plt.show()

    except:
        print('******************************')
        print('Error occurred during plotting '+filename)


print('Time to Run = ' + str(time.time()-starttime) + ' s')