##### 
##### Associated DSC functions
##### v5+
##### 
##### 

# Note: This file should not have to be run on its own. It contains the helper functions that are called in analyze_dsc_for_pub.py,
# reg_sol_model_get_L_from_binary.py, and reg_sol_model_get_eutectic.py. Each of those scripts calls the helper functions
# directly when needed.

import os
import platform
import pandas as pd
import numpy as np
import csv
from scipy import integrate
from scipy import signal
from scipy import interpolate
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import ast
import re

mainfolder = os.path.dirname(os.path.realpath(__file__))
datamainfolder = mainfolder+'/data'
plotsmainfolder = mainfolder+'/plots'
pythonversion = platform.python_version()

def import_DSC(folder,filename):
    # the dsc files tend to have null characters, need to remove them
    if pythonversion[0] == '3':
        with open(folder+'/'+filename,'r',errors='ignore') as simf:
            text = [a.replace('\0','').rstrip().split() for a in simf.readlines()]
            text = [b for b in text if b!=[]]
    else:
        with open(folder+'/'+filename,'r') as simf:
            text = [a.replace('\0','').rstrip().split() for a in simf.readlines()]
            text = [b for b in text if b!=[]]    
    # set the number of header lines to remove
    # currently two options, add more if more file types!
    try:
        headerlines=text.index(['StartOfData'])
    except ValueError:
        firstrows = [t[0] if len(t)>0 else '' for t in text]
        headerlines=firstrows.index('OrgFile') 
    dataintext = text[headerlines+1:-1]
    num_signals_idx = [i for i, item in enumerate(text) if re.search('Nsig', item[0])]
    num_signals = int(text[num_signals_idx[0]][1])
    # If the -2.000000 or -1.000000 lines aren't there, put them in   
    needlines = True
    for theline in text[0:headerlines+1]:
        thefullline = ' '.join(theline)
        if thefullline.find('Mark end of cycle') >= 0:
            needlines = False
    if needlines:          
        # Just do this by assuming a heat/cool/heat cycle where the lowest and highest temps
        # correspond to switches
        # Assume it gets at least 100 data points in before it's switching
        tempseries = [float(x[1]) for x in dataintext]
        temp_min_all = signal.argrelmin(np.array(tempseries),order=600)
        temp_min = [t for t in temp_min_all[0] if t > 100] # if a min found in first 100 data points, don't include
        temp_max_all = signal.argrelmax(np.array(tempseries),order=600)
        temp_max = [t for t in temp_max_all[0] if t > 100] # if a max found in first 100 data points, don't include
        indices_to_add = list(temp_min) + list(temp_max)
        offset = 0
        for i in indices_to_add:
            linetoadd = ['-2.000000'] + ['0.000000']*(1-num_signals)
            dataintext.insert(i+1+offset,linetoadd)
            offset += 1
        print('Note: added cycle changes at lines '+str([x+headerlines for x in indices_to_add]))
    # Import
    thecolumns = ['A','Temp','Heat'] + ['Sig_'+str(x) for x in range(4,num_signals+1)]
    data = pd.DataFrame(dataintext,columns=thecolumns,dtype=float)

    # Use the -2.000000 or -1.000000 lines to set the process (heat/cool/heat), starting at 1  
    data['Process'] = -1
    for i,p in enumerate([0]+data.index[(data['A'] == -2.0)|(data['A'] == -1.0)&(data['Heat'] == 0.0)].tolist()):
        data.loc[p:,'Process'] = i+1
    # then delete those rows
    data = data[(data['A'] != -2.0)&(data['Heat'] != 0.0)]
    return data


def import_spreadsheet(folder,filename):
    spreadsheet = pd.read_csv(folder+'/'+filename)  
    spreadsheet = spreadsheet.dropna(thresh=3)
    return spreadsheet


# Find first deviation from baseline
# direction is either inc for increasing or dec for decreasing
# baseline_coeff is the result of a linear fit
# baseline_range is given as e.g. [(0,10),(60,70)]
def deviation_baseline(df,baseline_coeff,baseline_range,num=3,dev=0.1,direction='inc'): 
    dev_from_baseline = []
    for i, row in df[(df.Temp>baseline_range[0][1])&(df.Temp<baseline_range[1][0])&(df.Process==num)].iterrows():
        if np.abs(row.Heat - np.polyval(baseline_coeff,row.Temp)) > dev:
            dev_from_baseline.append(row.Temp)
    if direction=='inc':
        return min(dev_from_baseline) 
    elif direction=='dec':
        return max(dev_from_baseline)


# Find baseline from the dataset
# Could use peakutils.baseline, but that didn't work great just for linear fits
# num : number of the process
# locations : tuple of start/stop boundaries
# returns coefficients
def fit_baseline_linear(df,num=3,location=(0,10)):
    return np.polyfit(df[(df.Process==num)&(df.Temp>location[0])&(df.Temp<location[1])].Temp,
        df[(df.Process==num)&(df.Temp>location[0])&(df.Temp<location[1])].Heat,deg=1)


# return value of the baseline at a list of points
# pts - the array-type list of points 
def baseline_vanderPlaats(df,pts,num=3,locations=[(0,10),(60,70)],Tb=None,Te=None):  
    linfit_b = fit_baseline_linear(df,num=num,location=locations[0])
    linfit_e = fit_baseline_linear(df,num=num,location=locations[1])
    if Tb == None:
        Tb = deviation_baseline(df,linfit_b,locations,num=num,dev=0.1,direction='inc')
    if Te == None:
        Te = deviation_baseline(df,linfit_e,locations,num=num,dev=0.1,direction='dec')
    baseline=[]
    k = 10./(Te-Tb)
    x0 = (Te+Tb)/2.
    def alpha(T):
        return 1./(1.+np.exp(-k*(T-x0)))
    # normalization parameters to get a smooth transition
    norm1 = alpha(Tb)
    norm2 = alpha(Te) - norm1

    for pt in pts:
        if pt<=Tb:
            baseline.append(np.polyval(linfit_b,pt))
        elif pt>Tb and pt<Te:
            baseline.append(np.polyval(linfit_b,pt)+(np.polyval(linfit_e,pt)-np.polyval(linfit_b,pt))*((alpha(pt)-norm1)/norm2))
        elif pt>=Te:
            baseline.append(np.polyval(linfit_e,pt))
    return baseline     
                 

def linear_fit_peak_sides(df,num=3,peaks=[0],fit_nums={'left':1,'right':1},s=0.001,return_temps=False):
    """Calculate slope in a linear region to either side of a certain peak.

    Finds where the second derivative crosses 0 to either side of the peak (given as a list of temperatures)
    Returns linear fits for both sides, given as arrays of coefficients (higher power first, as in np.polyfit).
    
    Parameters
    ----------
    df : dataframe of DSC measurements
    num : process, default 3
    peaks : list of peaks
    fit_nums : dict, which crossing of 0 by the 2nd derivative to choose, e.g. 1 is the first 
        crossing closest to the peak, 2 is the second closest, etc., for left and right peaks, 
        default {'left':1,'right':1}, applies to all peaks
    s : scale for stepping through 2nd derivative to find where it crosses 0, default 0.001
    return_temps : if True, returns temperatures of the fits, default False
    
    Returns
    ----------
    fits : returns an array of [left fit, right fit] where each fit is in the form [m, y1-m*x1]
    temps : if return_temps is True, also returns the temperatures where the fits are calculated
    """


    # construct a dfslice with the process we care about, which
    # has a temp that is only increasing (for the spline)
    # start 100 data points in in case there is a non-monotonic temp rise at the beginning
    dfslice = df[df.Process==num][100:]
    # drop any other points that are not increasing
    while len(dfslice[dfslice.Temp.diff()<0])>0:
        pointstodrop = dfslice[dfslice.Temp.diff()<0]
        dfslice.drop(pointstodrop.index,inplace=True)
        print('Note in linear_fit_peak_sides(): dropped '+str(len(pointstodrop))+' points where the temperature was not monotonically increasing.')


    # spline
    thespline = interpolate.UnivariateSpline(dfslice.Temp, dfslice.Heat,k=2,s=s)
        
    thespline_1d = thespline.derivative(n=1)
    thespline_2d = thespline.derivative(n=2)

    fits = []
    temps = []
    for thepeak in peaks:
        # find the temperatures where the 2nd derivative crosses 0
        spline_loc = {'left':thepeak, 'right':thepeak}
        dataframe_loc = {}
        slope = {}
        found = {'left':False, 'right':False}  
        # assume start with negative curvature - peak pointing down
        sign = {'left':-1, 'right':1}
        for side in ('left','right'):
            n = 0
            fit_num_found = 0
            switch_sign = 1
            while not found[side]:
                # start at peak and step through until the second derivative goes negative
                if thespline_2d(spline_loc[side] + sign[side]*n*s)*switch_sign < 0:
                    # if fit_nums > 1 are specified, then on each fit found < fit_nums,
                    # continue and switch the sign of the 2nd derivative crossing since we
                    # want to wait for it to go positive again, and once it does that,
                    # we switch the sign back and continue to look for the next fit where it goes negative
                    if switch_sign == -1:
                        switch_sign = 1
                        continue
                    fit_num_found += 1
                    if fit_num_found != fit_nums[side]:
                        switch_sign = -1
                        continue
                    # once found, remember where we are
                    spline_loc[side] = spline_loc[side] + sign[side]*n*s
                    # the slope is the first derivative at that location
                    slope[side] = float(thespline_1d(spline_loc[side]))
                    # find the closest temperature in the dataframe to these points
                    # note - the index returned is only valid for the df slice
                    dataframe_loc[side] = (dfslice.Temp-spline_loc[side]).abs().argsort().values[0]
                    found[side] = True
                    print('Found '+side+' at n='+str(n)+', temp='+str(spline_loc[side]))
                n += 1
        
        # format: [m, y1-m*x1]
        fits.append([[slope['left'], dfslice.iloc[dataframe_loc['left']].Heat-slope['left']*dfslice.iloc[dataframe_loc['left']].Temp],[slope['right'], dfslice.iloc[dataframe_loc['right']].Heat-slope['right']*dfslice.iloc[dataframe_loc['right']].Temp]])
        temps.append([spline_loc['left'],spline_loc['right']])
    if return_temps:
        return fits,temps
    else:
        return fits


#def half_fit_peak_sides(df,num=3,peaks=[0],return_temps=False):
#    """Calculate average slope to either side of a certain peak, from half-max to max."""
#    dfslice = df[df.Process==num]
#    # start 100 data points in in case there is a non-monotonic temp rise at the beginning
#    # spline
#    thespline = interpolate.UnivariateSpline(dfslice.Temp[100:], dfslice.Heat[100:],k=2,s=s)
#
#    fits = []
#    temps = []
#    for thepeak in peaks:
#        for side in ('left','right'):
#            n = 0
#            while not found[side]:
#                if thespline_2d(spline_loc[side] + sign[side]*n*s) < 0:
#                    spline_loc[side] = spline_loc[side] + sign[side]*n*s
#                    slope[side] = float(thespline_1d(spline_loc[side]))
#                    # find the closest temperature in the dataframe to these points
#                    # note - the index returned is only valid for the df slice
#                    dataframe_loc[side] = (dfslice.Temp-spline_loc[side]).abs().argsort().values[0]
#                    found[side] = True
#                n += 1
#        
#        # format - [m, y1-m*x1]
#        fits.append([[slope['left'], dfslice.iloc[dataframe_loc['left']].Heat-slope['left']*dfslice.iloc[dataframe_loc['left']].Temp],[slope['right'], dfslice.iloc[dataframe_loc['right']].Heat-slope['right']*dfslice.iloc[dataframe_loc['right']].Temp]])
#        temps.append([spline_loc['left'],spline_loc['right']])
#    
#    
#    
#    
#    if return_temps:
#        return fits,temps
#    else:
#        return fits    



# Find melting point, given a baseline and a linear fit region - mp is the intersection of the two
# fit_loc is a tuple with the start and end of that region
def calc_mp(df,fit_loc,baseline_coeff,num=3):
    fit_coeff = np.polyfit(df[(df.Process==num)&((df.Temp>fit_loc[0])&(df.Temp<fit_loc[1]))].Temp,
    df[(df.Process==num)&((df.Temp>fit_loc[0])&(df.Temp<fit_loc[1]))].Heat,deg=1)
    mp_arr = np.roots(fit_coeff-baseline_coeff)
    return mp_arr[0], fit_coeff


# Find the intersection between the baseline and a fit of the mp peak
# assumes only one crossing...
def mp_from_baseline_and_fit(x,baseline,mp_fit,lock_to_datapoint=False):
    x = list(x)
    fit_pts = np.polyval(mp_fit,x)
    idx = np.argwhere(np.diff(np.sign(baseline - fit_pts))).flatten() 
    if not lock_to_datapoint:
        baseline_linearized = np.polyfit([x[idx[0]],x[idx[0]+1]],[baseline[idx[0]],baseline[idx[0]+1]],deg=1)    
        # intersection of two linear fits
        mp = (mp_fit[1]-baseline_linearized[1])/(baseline_linearized[0]-mp_fit[0])
        return mp
    else:
        return x[idx[0]]


# Find enthalpy by integrating the heat curve to the baseline
# peak_loc is a tuple with the start and end of the region to integrate
# return J/mol
# moles - number of moles of the sample
# ramp - ramp rate in degC/min
def calc_enthalpy_linear(df,peak_loc,baseline_coeff,moles,ramp,num=3):
    baseline_values = np.polyval(baseline_coeff,df[(df.Process==num)&((df.Temp>peak_loc[0])&(df.Temp<peak_loc[1]))].Temp)
    return integrate.simps(df[(df.Process==num)&((df.Temp>peak_loc[0])&(df.Temp<peak_loc[1]))].Heat - baseline_values,
    x=df[(df.Process==num)&((df.Temp>peak_loc[0])&(df.Temp<peak_loc[1]))].Temp)/moles/1000.*60./ramp


# Find enthalpy by integrating the heat curve to the baseline
# peak_loc is a tuple with the start and end of the region to integrate
# return J/mol
# moles - number of moles of the sample
def calc_enthalpy_vanderPlaats(df,peak_loc,moles,ramp,num=3,locations=[(0,10),(60,70)],baseline_values=None):

    if baseline_values == None:
        baseline_values = baseline_vanderPlaats(df,df[(df.Process==num)&((df.Temp>peak_loc[0])&(df.Temp<peak_loc[1]))].Temp,num=num,locations=locations)

    return integrate.simps(df[(df.Process==num)&((df.Temp>peak_loc[0])&(df.Temp<peak_loc[1]))].Heat - baseline_values,
    x=df[(df.Process==num)&((df.Temp>peak_loc[0])&(df.Temp<peak_loc[1]))].Temp)/moles/1000.*60./ramp


# Find the enthalpy of a peak by integrating the heat curve to the baseline
# start_temp is the start of integration
# lin_fits and lin_fit_temps are outputs from linear_fit_peak_sides
# baseline_props is a dict that includes the properties needed by the baseline_vanderPlaats function 
# return_pts will also return all of the points used    
# returned enthalpy in J (not normalized to moles, because we don't know how many moles)
def calc_enthalpy_peak(df,start_temp,lin_fits,lin_fit_temps,baseline_props,ramp,num=3,return_pts=False):
   
    enthalpy = 0
    
    # construct the enthalpy from two parts, the left side (follow data), right side (linear fit)
    # temperature at which the integration switches is the temp of the inflection point
    temp_switch = lin_fit_temps[0][1]

    # left side
    temp_points_left = df[(df.Process==num)&((df.Temp>start_temp)&(df.Temp<temp_switch))].Temp
    signal_points_left = df[(df.Process==num)&((df.Temp>start_temp)&(df.Temp<temp_switch))].Heat
    baseline_points_left = baseline_vanderPlaats(df,temp_points_left,num=num,locations=baseline_props['locations'],Tb=baseline_props['Tb'],Te=baseline_props['Te'])
    #integrate left
    enthalpy += integrate.simps(signal_points_left - baseline_points_left,x=temp_points_left)/1000.*60./ramp
  
    # right side
    x_foo = df[(df.Process==num)&((df.Temp>baseline_props['Tb'])&(df.Temp<baseline_props['Te']))].Temp
    end_temp = mp_from_baseline_and_fit(x_foo,baseline_vanderPlaats(df,x_foo,num=num,locations=baseline_props['locations'],Tb=baseline_props['Tb'],Te=baseline_props['Te']),
                                           lin_fits[0][1],lock_to_datapoint=True)
    temp_points_right = df[(df.Process==num)&((df.Temp>temp_switch)&(df.Temp<end_temp))].Temp
    signal_points_right = np.polyval(lin_fits[0][1],temp_points_right)
    baseline_points_right = baseline_vanderPlaats(df,temp_points_right,num=num,locations=baseline_props['locations'],Tb=baseline_props['Tb'],Te=baseline_props['Te'])
    # integrate right
    enthalpy += integrate.simps(signal_points_right - baseline_points_right,x=temp_points_right)/1000.*60./ramp

    if not return_pts:
        return enthalpy
    else:
        return enthalpy,(list(temp_points_left)+list(temp_points_right),list(baseline_points_left)+list(baseline_points_right),list(signal_points_left)+list(signal_points_right))

# Find the peaks in a given process
# Uses the scipy function find_peaks
# Returns a list of the temperature values of the peaks
# return J
def find_peaks(df,num=3,prominence=0.025,return_properties=False):  
    # only find negative pointing peaks
    peaks_loc, properties = signal.find_peaks(-df[df.Process==num].Heat,prominence=prominence,threshold=0)
    peaks_temp = df[df.Process==num].iloc[peaks_loc].Temp.values
    if return_properties:
        return peaks_temp, peaks_loc, properties
    else:
        return peaks_temp


def DSC_plot_overlay(filenames,spreadsheet,molnormalized=False,save=False,filepath='folder/filename.jpg'):

    # now assumes that all the data is in a separate folder called 'data'
    mainfolder = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))+'/data'
    
    plt.close('all')
    plt.figure(1,figsize=(4,4), dpi=200)
    axes = plt.gca()

    numcurves = len(filenames)
    colors = cm.Set2(np.linspace(0, 1, numcurves))

    for i,thefile in enumerate(filenames):
        #baseline_fitting_points = ast.literal_eval(spreadsheet[spreadsheet.Filename==thefile].baseline_fitting_points.values[0])
        #mp_fitting_points = ast.literal_eval(spreadsheet[spreadsheet.Filename==thefile].mp_fitting_points.values[0])
        #enthalpy_fitting_points = ast.literal_eval(spreadsheet[spreadsheet.Filename==thefile].enthalpy_fitting_points.values[0])       
        if pd.isnull(spreadsheet[spreadsheet.Filename==thefile].Process.values[0]):
            process = 3
        else:
            process = spreadsheet[spreadsheet.Filename==thefile].Process.values[0]

        filefolder=str(int(spreadsheet[spreadsheet.Filename==thefile].DateFolder.values[0]))
        data = import_DSC(mainfolder+'/'+filefolder,thefile)
        #baseline_coeff = fit_baseline(data,locations=baseline_fitting_points,num=process)
        #mp, mp_fit_coeff = calc_mp(data,mp_fitting_points,baseline_coeff,num=process)
        if molnormalized:
            moles = spreadsheet[spreadsheet.Filename==thefile].Sample_mg.values[0]/1000./spreadsheet[spreadsheet.Filename==thefile].MW_avg.values[0]
            axes.plot(data[data.Process==process].Temp,[a/moles/1000. for a in data[data.Process==process].Heat],linewidth=1,c=colors[i],label=thefile[0:-4])
        else:
            axes.plot(data[data.Process==process].Temp,data[data.Process==process].Heat,linewidth=1,c=colors[i],label=thefile[0:-4])
            
        #ramp = spreadsheet[spreadsheet.Filename==thefile].Ramp.values[0]
        #enthalpy = calc_enthalpy(data,enthalpy_fitting_points,baseline_coeff,moles,ramp,num=process)        
        #plt.plot(data[data.Process==process].Temp,np.polyval(baseline_coeff,data[data.Process==process].Temp),'--',linewidth=1,c='orange')
    
        #baseline_points = [baseline_fitting_points[0][0],baseline_fitting_points[0][1],baseline_fitting_points[1][0],baseline_fitting_points[1][1]]
        #plt.plot(baseline_points,np.polyval(baseline_coeff,baseline_points),'|',c='orange')
    
        #plt.plot([mp]+[m for m in mp_fitting_points],np.polyval(mp_fit_coeff,[mp]+[m for m in mp_fitting_points]),'--',linewidth=1,c='yellow')
    
    # Labels
    axes.set_xlabel(r"Temp (C)")
    if molnormalized:
        axes.set_ylabel(r"Heat Flow ($10^3$ mW/mol)")
    else:
        axes.set_ylabel(r"Heat Flow (mW)")
    for item in ([axes.xaxis.label, axes.yaxis.label]):
        item.set_fontsize(9)
    for item in (axes.get_xticklabels() + axes.get_yticklabels()):
        item.set_fontsize(7)
    
    plt.legend(loc='lower right',fontsize=5)
    plt.gcf().subplots_adjust(left=0.15,bottom=0.15)
    
    if save:
        plt.savefig(filepath,format='jpg',dpi=300)
    
    plt.show()


